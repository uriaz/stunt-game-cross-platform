package com.minibit.stuntgame;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.minibit.box2dLevelEditor.BasePhysicsGame;
import com.minibit.gameUtils.BodyDescriptor;
import com.minibit.gameUtils.SmoothCamera;
import com.minibit.gameUtils.Tuple;
import com.minibit.sprites.MapSprite;
import com.minibit.sprites.MotorcycleSprite;
import com.minibit.sprites.ParralaxBackground;
import com.minibit.sprites.PhysicsSprite;
import com.minibit.sprites.PointF;

public class StuntGame extends BasePhysicsGame implements GameStateListener
{
	private SmoothCamera camera;
	private SpriteBatch batch;
	private Box2DDebugRenderer debugRenderer;
	private ShapeRenderer shapeRenderer;

	private MotorcycleSprite motorcycleSprite;
	private MapSprite map;
	private Sprite pathSprite;
	private String motorcycleImg = "dirtbike_red_rider.png", motorcyclePath = "dirtbike.tmx";
	private ParralaxBackground parralaxBackground;

	private String[] backgroundLevels =
	{
			"sky.png", "no_sky.png", "snowytrees.png"
	};

	private float[] backgroundLevelScrollSpeeds =
	{
			ParralaxBackground.NO_SCROLL, 2.0f, 4.25f
	};

	private PointF spriteStart;
	private Rectangle spriteSuccessArea;
	private GameTimer gameTimer;

	private float cameraStartZoom = 0.0f; // Zoom after zooming in initially
	private float mapMinY = 0.0f;

	private static final int ZOOM_ITERATIONS = 120;
	private int deviceSpecificZoomIterations = 0;
	private boolean shouldDebug = false;

	public StuntGame(String mapId)
	{
		super(mapId);
	}

	public StuntGame(String mapId, BroadLevelGameListener listener)
	{
		super(mapId, listener);
	}

	// Sprites that don't need to be recreated should be in here
	@Override
	public void create()
	{
		super.create();
		initCamera();

		batch = new SpriteBatch();
		debugRenderer = new Box2DDebugRenderer();
		shapeRenderer = new ShapeRenderer();
		shapeRenderer.setColor(Color.GRAY);
		Gdx.gl.glLineWidth(3.0f);

		initBackgrounds();
		// Instantiate a new matrix so we don't hold a reference to the camera's matrix
		gameTimer = new GameTimer("Skranji\\skranji.fnt", new Matrix4(camera.combined));
		reinitGame();
	}

	public void initCamera()
	{
		camera = new SmoothCamera();
		// Start zoom with respect to 800 x 400 screen size
		cameraStartZoom = camera.zoom / PhysicsSprite.PIXEL_METER_RATIO * 2.8f;

		// Scale for screens that aren't 800 x 400
		float mult = (float) 1.0 / (getScreenDims().x * getScreenDims().y / (800 * 400));
		cameraStartZoom *= Math.sqrt(mult);
		camera.setToOrtho(false, getScreenDims().x, getScreenDims().y);

		deviceSpecificZoomIterations = Gdx.app.getType().equals(ApplicationType.Android) ? (int) (ZOOM_ITERATIONS / 6.0)
				: ZOOM_ITERATIONS;
	}

	public void initBackgrounds()
	{
		pathSprite = new Sprite(new Texture(
				Gdx.files.internal("Sprite Image Data\\map\\path_texture.png")));
		pathSprite.setSize(pathSprite.getWidth(), pathSprite.getHeight() / 50);

		Tuple<String, Float>[] backgroundDescriptions = new Tuple[backgroundLevels.length];
		for (int i = 0; i < backgroundDescriptions.length; i++)
		{
			backgroundDescriptions[i] = new Tuple<String, Float>(backgroundLevels[i], 0.0f);
			backgroundDescriptions[i].setValue(backgroundLevelScrollSpeeds[i]);
		}

		parralaxBackground = new ParralaxBackground(backgroundDescriptions, camera.zoom,
				getScreenDims());
		parralaxBackground.setParralaxBackgroundMatrix(new Matrix4(camera.combined));
	}

	@Override
	public void reinitGame()
	{
		super.reinitGame();

		Sprite tSprite = new Sprite();
		tSprite.setSize(getScreenDims().x, getScreenDims().y);

		map = new MapSprite(tSprite, pathSprite, LEVEL_DATA_RESOURCE_PATH + getMapId(),
				getPhysicsWorld());
		map.createSpriteBody();
		mapMinY = map.getMapMinY();

		spriteStart = map.getSpriteEditor().getSpriteStartLoc();
		spriteSuccessArea = map.getSpriteEditor().getSpriteEndLoc();

		motorcycleSprite = new MotorcycleSprite(motorcycleImg, motorcyclePath, getPhysicsWorld());
		motorcycleSprite.createSpriteBody();
		motorcycleSprite.translatePhysicsSprite(spriteStart.x, spriteStart.y);
		/*
		 * If the motorcycle goes outside the rectangle specified by 0, mapMinY as the bottom left
		 * corner and the screen width, screen height as the upper right corner, the game should
		 * restart
		 */
		motorcycleSprite.setFailureBounds(this, new PointF(0, mapMinY), new PointF(
				getScreenDims().x, getScreenDims().y));
		motorcycleSprite.setSpriteSuccessArea(spriteSuccessArea);

		map.createActivationTriggerForSprite(this, motorcycleSprite);

		getGameEndTriggers().add(motorcycleSprite);
		getGameSprites().add(motorcycleSprite);
		getGameSprites().add(map);

		gameTimer.resetTimer();
	}

	@Override
	public void dispose()
	{
		super.dispose();
		batch.dispose();
	}

	private static final float ZOOM_SPEED = 1.005f;

	@Override
	public void render()
	{
		super.render();
		gameTimer.updateTimer();

		camera.update();
		parralaxBackground.update(camera, spriteStart);
		followSprite(motorcycleSprite);
		completeCameraCalculations();

		parralaxBackground.render(batch);

		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		for (int i = 0; i < getGameSprites().size(); i++)
		{
			getGameSprites().get(i).draw(batch);
		}
		map.renderPathTexture(batch);
		batch.end();

		shapeRenderer.setProjectionMatrix(camera.combined);
		map.renderDebugPath(shapeRenderer);

		if (shouldDebug)
		{
			debugRenderer.render(getPhysicsWorld(), camera.combined);
		}

		float textWidth = gameTimer.getTextWidth();
		float textHeight = gameTimer.getTextHeight();
		gameTimer.drawTime(batch, getScreenDims().x - (textWidth * 1.3f), getScreenDims().y
				- (textHeight * 1.3f));
	}

	private PointF p1Old = new PointF();
	private PointF p2Old = new PointF();

	public void completeCameraCalculations()
	{
		if (Gdx.input.isButtonPressed(Buttons.LEFT) && Gdx.input.isKeyPressed(Keys.M)
				|| Gdx.input.isTouched() && isGamePaused())
		{
			float dx = -Gdx.input.getDeltaX();
			float dy = Gdx.input.getDeltaY();
			float zoomFactor = camera.zoom * 20;
			camera.translate(dx / PhysicsSprite.PIXEL_METER_RATIO * zoomFactor, dy
					/ PhysicsSprite.PIXEL_METER_RATIO * zoomFactor);
		}

		if (Gdx.app.getType().equals(Application.ApplicationType.Desktop))
		{
			if (Gdx.input.isKeyPressed(Keys.UP))
			{
				camera.setFinishedZooming(true);
				zoomClicked(true, 1.09f);
			}
			else if (Gdx.input.isKeyPressed(Keys.DOWN))
			{
				camera.setFinishedZooming(true);
				zoomClicked(false, 1.09f);
			}
		}
		else if (Gdx.app.getType().equals(ApplicationType.Android) && isGamePaused())
		{
			if (Gdx.input.isTouched(1))
			{
				float p1X = Gdx.input.getX(0);
				float p1Y = Gdx.input.getY(0);
				float p2X = Gdx.input.getX(1);
				float p2Y = Gdx.input.getY(1);
				float dx = Math.abs(p2X - p1X);
				float dy = Math.abs(p2Y - p1Y);
				float resul = (float) Math.sqrt(dx * dx + dy * dy);

				float p1XOld = p1Old.x;
				float p1YOld = p1Old.y;
				float p2XOld = p2Old.x;
				float p2YOld = p2Old.y;
				float dxO = Math.abs(p2XOld - p1XOld);
				float dyO = Math.abs(p2YOld - p1YOld);
				float resulO = (float) Math.sqrt(dxO * dxO + dyO * dyO);

				camera.setFinishedZooming(true);
				if (resul > resulO)
				{
					zoomClicked(true, 1.05f);
				}
				else
				{
					zoomClicked(false, 1.05f);
				}

				p1Old.set(p1X, p1Y);
				p2Old.set(p2X, p2Y);
			}
		}

		if (!isGamePaused())
		{
			if (Gdx.input.isTouched() || Gdx.input.isKeyPressed(Keys.RIGHT)
					|| Gdx.input.isKeyPressed(Keys.LEFT))
			{
				camera.setSmoothZoom(cameraStartZoom * 1.25f, deviceSpecificZoomIterations);
			}
			else
			{
				// If screen isn't touched, maintain this distance away from bike
				camera.setSmoothZoom(cameraStartZoom / 1.25f, deviceSpecificZoomIterations);
			}
		}
	}

	@Override
	public void activationTriggered(PhysicsSprite spriteTriggered)
	{
//		System.out.println("Activation triggered! "
//				+ ((BodyDescriptor) spriteTriggered.getMainSpriteBody().getUserData()).getBodyId()
//				+ ", " + spriteTriggered.getX());
	}
	
	private boolean updateSuccess = true;
	@Override
	public void spriteReachedSuccessArea(PhysicsSprite sprite)
	{
		if (sprite == motorcycleSprite && updateSuccess)
		{
			getBroadLevelGameListener().clearedLevel(gameTimer.getElapsedTime());
			gameTimer.pause(true);
			updateSuccess = false;
		}
	}

	public void followSprite(PhysicsSprite spriteToFollow)
	{
		if (spriteToFollow != null && spriteToFollow.getMainSpriteBody() != null)
		{
			float x = spriteToFollow.getMainSpriteBody().getWorldCenter().x;
			float y = spriteToFollow.getMainSpriteBody().getWorldCenter().y;
			float dx = x - camera.position.x;
			float dy = y - camera.position.y;
			if (!Gdx.input.isKeyPressed(Keys.M))
			{
				camera.translate(dx, dy);
				parralaxBackground.getParralaxBackgroundMatrix().translate(-dx, 0, 0);
			}
		}
	}

	@Override
	public void zoomClicked(boolean zoomIn)
	{
		if (zoomIn)
		{
			camera.zoom = camera.zoom / (ZOOM_SPEED * 1.2f);
		}
		else
		{
			camera.zoom = camera.zoom * (ZOOM_SPEED * 1.2f);
		}

		parralaxBackground.zoomChanged(camera.zoom);
	}

	public void zoomClicked(boolean zoomIn, float zoomSpeed)
	{
		if (zoomIn)
		{
			camera.zoom = camera.zoom / (zoomSpeed);
		}
		else
		{
			camera.zoom = camera.zoom * (zoomSpeed);
		}

		parralaxBackground.zoomChanged(camera.zoom);
	}

	@Override
	public void debugButtonClicked()
	{
		shouldDebug = !shouldDebug;
	}

	@Override
	public void gameStateChanged(boolean isPaused)
	{
		super.setGamePaused(isPaused);
		gameTimer.pause(isPaused);
	}

	@Override
	public void resize(int width, int height)
	{
		super.resize(width, height);
	}
}
