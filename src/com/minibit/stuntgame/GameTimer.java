package com.minibit.stuntgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;

public class GameTimer
{
	private int elapsedTime;
	private long startTime = System.currentTimeMillis();
	private String timeDisplay = "0.0";
	private BitmapFont drawFont;
	private Matrix4 nonTranslatedMatrix;

	/**
	 * 
	 * @param fontFile
	 *            File for font to use while drawing timer text
	 * @param nonTranslatedMatrix
	 *            Default matrix that isn't affected by translation / rotation / scaling operations
	 *            of camera
	 */
	public GameTimer(String fontFile, Matrix4 nonTranslatedMatrix)
	{
		drawFont = new BitmapFont(Gdx.files.internal(fontFile), false);
		this.nonTranslatedMatrix = nonTranslatedMatrix;
	}

	public GameTimer(Matrix4 nonTranslatedMatrix)
	{
		drawFont = new BitmapFont();
		this.nonTranslatedMatrix = nonTranslatedMatrix;
	}

	/**
	 * 
	 * @param dt
	 *            Change in time from last update (milliseconds)
	 */
	private void updateTimer(int dt)
	{
		elapsedTime += dt;
		double display = elapsedTime / 1000.0;
		// Truncate milliseconds to 1 decimal place
		int seconds = (int) display;
		int milliProportion = (int) ((display - seconds) * 1000);
		int tenthDecimal = milliProportion / 100;
		timeDisplay = seconds + "." + tenthDecimal;
	}

	public void updateTimer()
	{
		if (!isPaused)
		{
			long newTime = System.currentTimeMillis();
			int dt = (int) (newTime - startTime);
			startTime = newTime;
			updateTimer(dt);
		}
	}

	private boolean isPaused = false;

	public void pause(boolean shouldPause)
	{
		isPaused = shouldPause;
		if (!isPaused)
		{
			startTime = System.currentTimeMillis();
		}
	}

	public void drawTime(SpriteBatch batch, float x, float y)
	{
		batch.setProjectionMatrix(this.nonTranslatedMatrix);
		batch.begin();
		drawFont.draw(batch, timeDisplay, x, y);
		batch.end();
	}

	public void resetTimer()
	{
		elapsedTime = 0;
	}

	// Use width / height that are calculated only once for performance benefits
	private float defaultTextWidth, defaultTextHeight;

	public float getTextWidth()
	{
		if (defaultTextWidth == 0.0f)
		{
			defaultTextWidth = drawFont.getBounds("00.0").width;
		}
		return defaultTextWidth;
	}

	public float getTextHeight()
	{
		if (defaultTextHeight == 0.0f)
		{
			defaultTextHeight = drawFont.getBounds("00.0").height;
		}
		return defaultTextHeight;
	}

	public String getTimeDisplay()
	{
		return timeDisplay;
	}

	public void setTimeDisplay(String timeDisplay)
	{
		this.timeDisplay = timeDisplay;
	}

	public Matrix4 getNonTranslatedMatrix()
	{
		return nonTranslatedMatrix;
	}

	public void setNonTranslatedMatrix(Matrix4 nonTranslatedMatrix)
	{
		this.nonTranslatedMatrix = nonTranslatedMatrix;
	}

	public BitmapFont getDrawFont()
	{
		return drawFont;
	}

	public void setDrawFont(BitmapFont drawFont)
	{
		this.drawFont = drawFont;
	}

	public int getElapsedTime()
	{
		return elapsedTime;
	}

	public void setElapsedTime(int elapsedTime)
	{
		this.elapsedTime = elapsedTime;
	}
}
