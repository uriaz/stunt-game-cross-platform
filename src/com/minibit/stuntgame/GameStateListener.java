package com.minibit.stuntgame;

public interface GameStateListener
{
	public void gameStateChanged(boolean isPaused);

	public void zoomClicked(boolean zoomIn);

	public void restartGameClicked();

	public void debugButtonClicked();
}
