package com.minibit.sprites;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint;
import com.minibit.gameUtils.BodyDescriptor;

public class MotorcycleSprite extends PhysicsSprite
{
	public MotorcycleSprite(String spriteImagePath, String spriteLvEditorPath, World physicsWorld)
	{
		super(spriteImagePath, spriteLvEditorPath, physicsWorld);
	}

	private RevoluteJoint frontWheelMotor, backWheelMotor;
	private Body frontWheel, backWheel;

	@Override
	public void initSprite()
	{
		backWheelMotor = (RevoluteJoint) getJointForId("backWheelConnection");
		frontWheelMotor = (RevoluteJoint) getJointForId("frontWheelConnection");

		Iterator<Body> bodyList = getPhysicsWorld().getBodies();
		while (bodyList.hasNext())
		{
			Body body = (Body) bodyList.next();
			BodyDescriptor descriptor = (BodyDescriptor) body.getUserData();
			if (descriptor != null && descriptor.getBodyId() != null)
			{
				if (descriptor.getBodyId().equals("Back Wheel"))
				{
					backWheel = body;
				}
				else if (descriptor.getBodyId().equals("Front Wheel"))
				{
					frontWheel = body;
				}
			}
		}
	}

	private static final float maxMotorSpeed = (float) (Math.PI * 4);
	private static final float maxAngularVelocity = 0.52f;
	private static final float superChargeForce = 1.3f;
	private static final float maxBodyTorque = 0.32f;
	float angle = 0;

	@Override
	public void handleUserInput()
	{
		super.handleUserInput();
		if (getMainSpriteBody() != null)
		{
			float currentAngVel = Math.abs(getMainSpriteBody().getAngularVelocity());
			if (currentAngVel > maxAngularVelocity)
			{
				getMainSpriteBody().setAngularVelocity(maxAngularVelocity);
			}

			if (backWheelMotor != null)
			{
				switch (Gdx.app.getType())
				{
				case Android:
					if (Gdx.input.isTouched())
					{
						float x = Gdx.input.getX();
						float reverseLimit = getSpriteGameBounds().width * 0.35f;
						if (x > reverseLimit)
						{
							frontWheel.setAngularVelocity(-maxMotorSpeed);
							backWheel.setAngularVelocity(-maxMotorSpeed);
						}
						else
						{
							// Apply brakes if touch was in left half of screen
							frontWheel.setAngularVelocity(0);
							backWheel.setAngularVelocity(0);
						}
					}

					float accY = Gdx.input.getAccelerometerY();
					float accX = Gdx.input.getAccelerometerX();
					float angle = (float) (Math.atan2(accX, accY) * 180.0 / Math.PI);
					angle = 90.0f - angle;

					if (currentAngVel < maxAngularVelocity && accY > 0)
					{
						if (Math.abs(angle) > 8)
						{
							getMainSpriteBody().applyTorque(-maxBodyTorque);
						}
					}
					else if (currentAngVel < maxAngularVelocity && accY < 0)
					{
						if (Math.abs(angle) > 8)
						{
							getMainSpriteBody().applyTorque(maxBodyTorque);
						}
					}
					break;

				case Desktop:
					if (Gdx.input.isKeyPressed(Keys.RIGHT))
					{
						frontWheel.setAngularVelocity(-maxMotorSpeed);
						backWheel.setAngularVelocity(-maxMotorSpeed);
					}
					else if (Gdx.input.isKeyPressed(Keys.LEFT))
					{
						frontWheel.setAngularVelocity(0);
						backWheel.setAngularVelocity(0);
					}
					if (Gdx.input.isKeyPressed(Keys.A))
					{
						if (currentAngVel < maxAngularVelocity)
						{
							getMainSpriteBody().applyTorque(maxBodyTorque * 4);
						}
					}
					else if (Gdx.input.isKeyPressed(Keys.D))
					{
						if (currentAngVel < maxAngularVelocity)
						{
							getMainSpriteBody().applyTorque(-maxBodyTorque * 4);
						}
					}
					else if (Gdx.input.isKeyPressed(Keys.SPACE))
					{
						getMainSpriteBody().applyForceToCenter(superChargeForce, 0.0f);
					}

					break;

				default:
					break;
				}
			}
		}
	}
}
